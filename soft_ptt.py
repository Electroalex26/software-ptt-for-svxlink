# CONSTANTES
adresse="/root/falsegpio" # absolue idéalement

# FONCTIONS
def change():
  file = open(adresse, "r")
  raw = file.read()
  file.close()
  if raw == "0":
    val="1"
  else:
    val="0"
  file = open(adresse, "w")
  file.write(val)
  file.close()

def etat():
  file = open(adresse, "r")
  raw = file.read()
  file.close()
  if raw == "0":
    return "A l'écoute du réseau SVXLink"
  else:
    return "TX !"

# PROGRAMME
adresse += "/value"
try:
  while True:
    print(etat())
    input()
    change()
except KeyboardInterrupt:
  file = open(adresse, "w")
  file.write("0")
  file.close()
  print(" Arret ...")

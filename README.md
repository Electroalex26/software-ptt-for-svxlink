# Software PTT for SVXLink

## REQUIS :
* svxlink
* python3

## INSTALLATION :
* Copiez le dossier "falsegpio" à l'emplacement que vous voulez.
* Modifiez l'adresse du dossier dans la première ligne du programme python.
* Modifiez le fichier de configuration de svxlink dans la partie RX1 :
```ini
SQL_DET=GPIO
GPIO_PATH= [Mettez l'adresse du répertoire PARENT à "falsegpio"]
GPIO_SQL_PIN=falsegpio
```

Si l'adresse du répertoire "falsegpio" est "/root/falsegpio" ...  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;... alors le début du programme python est :  
```python
adresse="/root/falsegpio"
```
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;... alors le fichier de conf RX1 donne :  
```ini
SQL_DET=GPIO
GPIO_PATH=/root
GPIO_SQL_PIN=falsegpio
```

## UTILISATION :
   - Lancez svxlink
   - Lancez le programme python en faisant dans un terminal : "python3 soft_ptt.py"
   - Par défaut vous écoutez le réseau SVXLink
   - Pour commencer à parler, appuyez sur "Entrée"
   - Le programme vous affiche "TX !", vous pouvez parler
   - Pour arrêter de parler, appuyez de nouveau sur "Entrée"
   - Le programme vous confirme que vous êtes bien repassé en écoute du réseau
   - Pour parler de nouveau, refaites "Entrée" ... etc
   - Pour fermer le programme, faites Ctrl + C
